# H0022 - AvE iMX8MP - AP1302 - demo based on LDK - manifest

this repo contains the manifest(s) for building `AvE iMX8MP AP1302 EdgeAI` based on `AvE LDK`

## pre-requisites

- you need [kas](https://github.com/siemens/kas) installed: run `pip3 install kas`
- access to [avnet-embedded's GitHub](https://github.com/avnet-embedded/)
- add your SSH public key (default: ~/.ssh/id_rsa.pub) to [your GitHub profile](https://github.com/settings/keys)

### if you haven't run a checkout via ssh from GitHub yet...

```shell
cd /tmp
git clone ssh://git@github.com/avnet-embedded/meta-avnet-embedded-simpleswitch-container-examples.git

# answer question with yes

rm -rf meta-avnet-embedded-simpleswitch-container-examples
```

## build

- clone this repo
- run `kas build h0022-witekio-edge-ai-ldk-manifest/witekio-edge-ai-ldk.yml`
- resulting images can be found at `build/tmp/deploy/images/*/*.wic`

### build in a container

For newer host distributions it's advised to run the build in a container

- install `docker-ce` or `podman`
- `sudo setfacl --modify user:<your user name>:rw /var/run/docker.sock`
- `./kas-container build --ssh-d $HOME/.ssh witekio-edge-ai-ldk.yml`

### get a bitbake console

- run `kas shell h0022-witekio-edge-ai-ldk-manifest/witekio-edge-ai-ldk.yml`

### check the kas documentation

for more kas options, please check the [documentation](https://kas.readthedocs.io/en/latest/userguide.html)

## build a release

the file `h0022-witekio-edge-ai-ldk-manifest/witekio-edge-ai-ldk.yml` will always build the latest/greatest, so if you want to build a release please use any of `h0022-witekio-edge-ai-ldk-manifest/witekio-edge-ai-ldk-V*.yml`
